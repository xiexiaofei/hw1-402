#include "intMatrix.h"
#include <stdlib.h>
#include <time.h>

#define M1 900
#define N1 600
#define M2 600
#define N2 900


void productMatrix(void);
void generateMatrix1(int*** arr);
void generateMatrix2(int*** arr);
void dynamicArray(int*** arr, int m, int n);
void releaseArray(int*** arr, int x);

int main(){
    double sum = 0;
    for (int i = 0; i < 15; i++){
        clock_t beginTime,endTime;
        beginTime = clock();
        productMatrix();
        endTime = clock();
        printf("%fs\n", (double)(endTime-beginTime)/CLOCKS_PER_SEC);
        sum += (double)(endTime - beginTime)/CLOCKS_PER_SEC;
        printf("\n");
    }
    printf("After 15 times runing, the average rumtime is: %0.3fs ", sum/15);
    
    return 0;
}

void productMatrix(void) {
    int i, x, y, sum;
    int **a;
    int **b;

    int **res = (int **)malloc(M1 * sizeof(int *));
    for (int i = 0; i < M1; i++)
    {
        res[i] = (int *)malloc(N2 * sizeof(int));
    }

    generateMatrix1(&a);
    generateMatrix2(&b);
    dynamicArray(&res, M1, M1);

    for (y = 0; y < N2; y++){
        for (x = 0; x < M1; x++){
            sum = 0;
            for (i = 0; i < N1; i++){
                sum = sum + a[x][i] * b[i][y];
            }
            res[x][y] = sum;
        }
    }
//
//    //switch inner with outer loop
//   for (x = 0; x < M1; x++){
//        for (y = 0; y < N2; y++){
//            sum = 0;
//            for (i = 0; i < N1; i++){
//                sum = sum + a[x][i] * b[i][y];
//            }
//            res[x][y] = sum;
//        }
//    }
    
    releaseArray(&a, M1);
    releaseArray(&b, M2);
    releaseArray(&res, M1);
}

void generateMatrix2(int*** arr)
{
    int m = M2;
    int n = N2;
    int i = 0, j = 0;

    dynamicArray(arr, m, n);

    srand((unsigned)time(NULL));

    for (i = 0; i < m; ++i)
    {
        for (j = 0; j < n; ++j)
        {
            (*arr)[i][j] = rand() % 20;
        }
    }
}

void generateMatrix1(int*** arr)
{
    int m = M1;
    int n = N1;
    int i = 0, j = 0;

    dynamicArray(arr, m, n);

    srand((unsigned)time(NULL));

    for (i = 0; i < m; ++i)
    {
        for (j = 0; j < n; ++j)
        {
            (*arr)[i][j] = rand() % 20;
        }
    }

}


// new dynamic array
void dynamicArray(int*** arr, int m, int n)
{
    int i;
    *arr = (int **)malloc(sizeof(int *) * m);

    for(i = 0; i < m; i++)
    {
        (*arr)[i] = (int *)malloc(sizeof(int) * n);
        memset((*arr)[i], 0, sizeof(int) * n);
    }
}

//releas array
void releaseArray(int*** arr, int x)
{
    int i;

    for(i = 0; i < x; i++)
    {
        free((*arr)[i]);
        (*arr)[i] = 0;
    }

    free(*arr);
    *arr = 0;
}
